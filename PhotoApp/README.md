#  Photo App

*Take photo or bring photo from album in Swift, iOS*

## Copyright

© 2019 Jaesung. All Rights Reserved.

## Feature

### Permissions

1. Camera

    CameraUsageDescription

2. Photo Library

    PhotoLibraryUsageDescription

3. Save to Photo Library

    PhotoLibraryAdditionDescription

### Class

`UIImagePickerControllerDelegate` & `UINavigationControllerDelegate`


### Purpose

> **Case 1**: PhotoLibrary

> **Case 2**: Camera


### Properties

1. `UIImageView`

2. `UIImagePickerController`


### Methods

#### UINavigationControllerDelegate

1. `takePhoto()`

`UIImagePickerController.isSourceTypeAvailable(.camera)`

> `nil`: select image from photo library

> `exist`: select image from camera


2. `selectImageFrom(_ source: ImageSource)`

> **ImageSource**:
> ```Swift
> enum ImageSource {
>   case photoLibrary
>   case camera
> }

`imagePickerController.delegate = self`

`if source == .camera`

`imagePicker.sourceType = .camera`

`if source == .photoLibrary`

`imagePicker.sourceType = .photoLibrary`

`self.present()`


3. `save()`


4. `image(_:didFinishSavingWithError:contextInfo:)`

#### UIImagePickerControllerDelegate

5. `imagePickerController(_:didFinishPickerMediaWithInfo:)`
